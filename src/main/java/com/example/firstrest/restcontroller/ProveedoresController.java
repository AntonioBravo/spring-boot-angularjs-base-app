package com.example.firstrest.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.firstrest.model.Proveedor;
import com.example.firstrest.service.ProveedorService;

@RestController
@RequestMapping("/proveedores")
public class ProveedoresController {
	
	@Autowired
	private ProveedorService proveedorService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> consultaProveedores() {
		return new ResponseEntity<>(proveedorService.consultaProveedores(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{proveedorId}",method = RequestMethod.GET)
	public ResponseEntity<?> consultaProveedor(@PathVariable Integer proveedorId) {
		return new ResponseEntity<>(proveedorService.consultaProveedor(proveedorId), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> registraProveedor(@RequestBody Proveedor proveedor) {
		proveedor.setProveedorId(null);
		return new ResponseEntity<>(proveedorService.registraProveedor(proveedor),HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/{proveedorId}",method = RequestMethod.PUT)
	public ResponseEntity<?> actualizaProveedor(@RequestBody Proveedor proveedor, @PathVariable Integer proveedorId) {
		proveedor.setProveedorId(proveedorId);
		return new ResponseEntity<>(proveedorService.actualizaProveedor(proveedor), HttpStatus.OK);
	}	
	
	@RequestMapping(value = "/{proveedorId}", method = RequestMethod.DELETE)
	public ResponseEntity<?> eliminaProveedor(@PathVariable Integer proveedorId) {
		proveedorService.eliminaProveedor(proveedorId);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
