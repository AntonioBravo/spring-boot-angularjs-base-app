package com.example.firstrest.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.firstrest.model.Proveedor;
import com.example.firstrest.repository.ProveedorRepository;
import com.example.firstrest.service.ProveedorService;

@Service("proveedorService")
public class ProveedorServiceImpl implements ProveedorService {
	
	@Autowired
	private ProveedorRepository proveedorRepository;

	@Override
	public List<Proveedor> consultaProveedores() {
		return proveedorRepository.findAll();
	}

	@Override
	public Proveedor consultaProveedor(Integer proveedorId) {
		return proveedorRepository.findOne(proveedorId);
	}

	@Override
	public Proveedor registraProveedor(Proveedor proveedor) {
		return proveedorRepository.saveAndFlush(proveedor);
	}

	@Override
	public Proveedor actualizaProveedor(Proveedor proveedor) {
		return proveedorRepository.saveAndFlush(proveedor);
	}

	@Override
	public void eliminaProveedor(Integer proveedorId) {
		proveedorRepository.delete(proveedorId);
	}
	
	
	
}
