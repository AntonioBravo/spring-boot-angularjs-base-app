package com.example.firstrest.service;

import com.example.firstrest.model.User;

public interface UserService {
	public User findUserByEmail(String email);
	public void saveUser(User user);
}
