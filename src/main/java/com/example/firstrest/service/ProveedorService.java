package com.example.firstrest.service;

import java.util.List;

import com.example.firstrest.model.Proveedor;

public interface ProveedorService {
	
	public List<Proveedor> consultaProveedores();
	
	public Proveedor consultaProveedor(Integer proveedorId);
	
	public Proveedor registraProveedor(Proveedor proveedor);
	
	public Proveedor actualizaProveedor(Proveedor proveedor);
	
	public void eliminaProveedor(Integer proveedorId);

}
