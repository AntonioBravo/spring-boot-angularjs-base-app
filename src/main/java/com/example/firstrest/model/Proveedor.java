package com.example.firstrest.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "proveedor", catalog = "test")
public class Proveedor {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="proveedor_id", unique = true, nullable = false)
	private Integer proveedorId;
	
	@Column(name="nombre", nullable = false, length = 50)
	private String nombre;
	
	@Column(name="paterno", nullable = false, length = 50)
	private String paterno;
	
	@Column(name="materno", nullable = true, length = 50)
	private String materno;
	
	@Column(name="empresa", nullable = false, length = 50)
	private String empresa;

}
