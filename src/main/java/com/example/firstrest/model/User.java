package com.example.firstrest.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Transient;

import lombok.Data;

@Data
@Entity
@Table(name = "user", catalog = "test")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id")
	private int id;
	@Column(name = "email")
	@Email(message = "*Ingrese su usuario")
	@NotEmpty(message = "* Ingrese su usuario")
	private String email;
	@Column(name = "password")
	@Length(min = 5, message = "*Tu contraseña debe contener minimo 5 caracteres.")
	@NotEmpty(message = "*Ingrese su contraseña")
	@Transient
	private String password;
	@Column(name = "name")
	@NotEmpty(message = "*Ingrese su nombre")
	private String name;
	@Column(name = "last_name")
	@NotEmpty(message = "*Ingrese sus apellidos")
	private String lastName;
	@Column(name = "active")
	private int active;
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles;

}
